import {render} from 'react-dom'
import App from './pages/App'
import React from "react";

render(
    <App/>, document.getElementById('root')
);