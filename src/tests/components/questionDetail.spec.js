import {shallow} from "enzyme";
import {mount} from "enzyme";
import Footer from "../../components/footer"
import React from "react";
import Questions from "../../components/questions";

describe('[Components] - Questions', () => {
    test('should render correctly', () => {
        const node = mount(<Questions/>);
        expect(node.find('#questions').exists()).toEqual(true);
    })

    test('should render list of questionscorrectly', () => {
        const node = mount(<Questions/>);
        expect(node.find('.question').exists()).toEqual(true);
    })
});