import {shallow} from "enzyme";
import {mount} from "enzyme";
import Footer from "../../components/footer"
import React from "react";

describe('[Components] - Footer', () => {
    test('should render correctly', () => {
        const node = mount(<Footer/>);
        expect(node.find('#footer').exists()).toEqual(true);
        expect(node.find('.copyRight').exists()).toEqual(true)
    })
});