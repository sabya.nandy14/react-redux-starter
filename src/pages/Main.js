import React from 'react';
import CssBaseline from '@material-ui/core/CssBaseline';
import Footer from "../components/footer";
import Header from "../components/header";

export default function Questions() {


    return (
        <div>
            <CssBaseline/>
            <Header/>
            <Footer/>
        </div>
    );
}