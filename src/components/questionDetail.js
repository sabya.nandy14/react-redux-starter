import React from 'react'
import Typography from "@material-ui/core/Typography";
import Link from "@material-ui/core/Link";
import PropTypes from 'prop-types';
import {withStyles} from '@material-ui/core/styles';

const styles = {
    footer: {
        maxHeight: "10vh",
        backgroundColor: 'whitesmoke',
        padding: 3,
    },
};

function Copyright() {
    return (
        <Typography variant="body2" color="textSecondary" align="center">
            {'Copyright ©  '}
            <Link color="inherit" href="#">
                xyz@tw.com
            </Link>{' '}
            {new Date().getFullYear()}
            {'.'}
        </Typography>
    );
}

class Footer extends React.Component {
    render() {
        const {classes} = this.props;
        return (
            <footer id="footer" className={classes.footer}s>
                <Typography variant="h6" align="center" gutterBottom>
                    Footer
                </Typography>
                <Typography variant="subtitle1" align="center" color="textSecondary" component="p">
                    Something here to give the footer a purpose!
                </Typography>
                <Copyright className="copyRight"/>
            </footer>
        )
    }
}

Footer.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(Footer);